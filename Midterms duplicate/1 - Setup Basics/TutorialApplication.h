/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include "BaseApplication.h"
#include <OgreManualObject.h>
#include "planets.h"

using namespace Ogre;

//---------------------------------------------------------------------------
class planets;

class TutorialApplication : public BaseApplication
{
public:
	
    TutorialApplication(void);
    virtual ~TutorialApplication(void);
private:
	SceneNode* mercuryNode;
	SceneNode* venusNode;
	SceneNode* earthNode;
	SceneNode* moonNode;
	SceneNode* marsNode;
	SceneNode* sunNode;
protected:
	//virtual ManualObject* creatCube(float given, ColourValue colourOne, ColourValue colourTwo);
	virtual void createScene(void);
	bool frameStarted(const FrameEvent &evt);
	float x;
	float y;
	float z;
	SceneNode* cubeNode;
	planets* sun;
	planets* mercury;
	planets* venus;
	planets* earth;
	planets* mars;
	planets* moon;



};

//---------------------------------------------------------------------------

#endif // #ifndef __TutorialApplication_h_

//---------------------------------------------------------------------------
