/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)

http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/



#include "TutorialApplication.h"
#include "planets.h"

using namespace std;
using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
	//x = 150;
	//y = 0;
	//z = 150;
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	planets* Sun = new planets(sunNode);
	Sun->createPlanet(*mSceneMgr, 20, ColourValue(1,1,0) , 0, 0,sun,false);
	sun = Sun;

	planets* Mercury = new planets(mercuryNode);
	Mercury->createPlanet(*mSceneMgr, 3, ColourValue(127, 127, 127), 50, 50, sun, false);
	mercury = Mercury;

	planets* Venus = new planets(venusNode); 
	Venus->createPlanet(*mSceneMgr, 5, ColourValue::Red, 110, 110,sun,false);
	venus = Venus;

	planets* Earth = new planets(earthNode);
	Earth->createPlanet(*mSceneMgr, 10, ColourValue::Blue, 170, 170,sun,false);
	earth = Earth;


	planets* Mars = new planets(marsNode);
	Mars->createPlanet(*mSceneMgr, 8, ColourValue::White, 300, 300,sun,false);
	mars = Mars;

	planets* Moon = new planets(moonNode);
	Moon->createPlanet(*mSceneMgr, 1, ColourValue::White, 20 ,20,earth,true);
	moon = Moon;
}

bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	
	sun->setRotation(50.0f * evt.timeSinceLastFrame);

	mercury->updateRevolution(88, 6 * evt.timeSinceLastFrame);
	mercury->setRotation(100.0f * evt.timeSinceLastFrame);

	venus->updateRevolution(224, 6 * evt.timeSinceLastFrame);
	venus->setRotation(200.0f * evt.timeSinceLastFrame);

	
	earth->updateRevolution(365, 6 * evt.timeSinceLastFrame); //1 -  12 rotation per min
	earth->setRotation(300.0f * evt.timeSinceLastFrame);

	mars->updateRevolution(687,6 * evt.timeSinceLastFrame );
	mars->setRotation(250.0f * evt.timeSinceLastFrame);

	moon->updateRevolution(100,15 * evt.timeSinceLastFrame);
	moon->setRotation(100000 * evt.timeSinceLastFrame);


	return true;
}
//-----------Reference---------------
//https://www.gamefromscratch.com/post/2012/11/24/GameDev-math-recipes-Rotating-one-point-around-another-point.aspx
////https://stackoverflow.com/questions/47726891/how-to-revolve-an-object-around-another-object-c-and-ogre3d 

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif



	/*//FrontFaceBotRight
	cube->position(0, 0, 0);
	cube->position(20, 0, 0);
	cube->position(20, 20, 0);

	//FrontFaceTopLeft
	cube->position(20,20,0);
	cube->position(0,20,0);
	cube->position(0,0,0);
	------------------------------------------------front done
	//leftFacebotRight
	cube->position(0, 0, -20);
	cube->position(0, 0, 0);
	cube->position(0, 20, 0);

	//leftFacetopLeft
	cube->position(0, 20, 0);
	cube->position(0, 20, -20);
	cube->position(0, 0,-20);
	-----------------------------------------------  Left Done
	//RightFaceBotRight
	cube->position(20, 0, 0);
	cube->position(20, 0, -20);
	cube->position(20, 20, -20);

	//RightFaceTopLeft
	cube->position(20, 20, -20);
	cube->position(20, 20, 0);
	cube->position(20, 0, 0);

	---------------------------------------------- Right Done
	//BackFaceBotRight
	cube->position(0, 0, -20);
	cube->position(20, 20, -20);
	cube->position(20, 0, -20);


	//backFaceTopLeft
	cube->position(20, 20, -20);
	cube->position(0, 0, -20);
	cube->position(0, 20, -20);
	---------------------------------------------- Back DONE
	//topFacebotRight
	cube->position(0, 20, 0);
	cube->position(20, 20, 0);
	cube->position(20, 20, -20);

	//topFacetopLeft
	cube->position(20,20,-20);
	cube->position(0, 20, -20);
	cube->position(0, 20, 0);
	------------------------------------ top done

	//botFacebotRight
	cube->position(0, 0, 0);
	cube->position(20, 0, -20);
	cube->position(20, 0, 0);


	//botFacetopLeft
	cube->position(20, 0, -20);
	cube->position(0, 0, 0);
	cube->position(0, 0, -20);
	----------------------------- bot done
	*/


/*if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L))
{
	cubeNode->translate(x * evt.timeSinceLastFrame, 0, 0);
	x += 0.1;
}
if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J))
{
	cubeNode->translate(-x * evt.timeSinceLastFrame, 0, 0);
	x += 0.1;
}
if (mKeyboard->isKeyDown(OIS::KeyCode::KC_I))
{
	cubeNode->translate(0, x * evt.timeSinceLastFrame, 0);
	x += 0.1;
}
if (mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
{
	cubeNode->translate(0, -x * evt.timeSinceLastFrame, 0);
	x += 0.1;
}

//rotation

if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD8))
{
	Degree rotation = Degree(30 * evt.timeSinceLastFrame);
	cubeNode->rotate(Vector3(0, 1, 0), Radian(rotation));
}
if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD2))
{
	Degree rotation = Degree(30 * evt.timeSinceLastFrame);
	cubeNode->rotate(Vector3(0, -1, 0), Radian(rotation));
}
if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD4))
{
	Degree rotation = Degree(30 * evt.timeSinceLastFrame);
	cubeNode->rotate(Vector3(-1, 0, 0), Radian(rotation));
}
if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD6))
{
	Degree rotation = Degree(30 * evt.timeSinceLastFrame);
	cubeNode->rotate(Vector3(1, 0, 0), Radian(rotation));
}*/

//ManualObject* cube = mSceneMgr->createManualObject();

/*ManualObject* planets::createCube(float given, ColourValue GivenColour)
{
	ManualObject* cube = mSceneMgr->createManualObject();
	cube->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);

	//------front points-----------
	cube->position(-given, -given, given); // i-0 LL 
	cube->colour(colour);
	cube->position(given, -given, given); // i -1 LR
	cube->colour(colour);
	cube->position(given, given, given); // i -2 UR
	cube->colour(colour);
	cube->position(-given, given, given);// i - 3 UL
	cube->colour(colour);

	//----------- back points ----------------
	cube->position(-given, -given, -given); // i-4 LL
	cube->colour(colour);
	cube->position(given, given, -given); // i -5 UR
	cube->colour(colour);
	cube->position(given, -given, -given); // i - 6 LR
	cube->colour(colour);
	cube->position(-given, given, -given); // i - 7 UL
	cube->colour(colour);
	//------front face------------------
	cube->index(0);
	cube->index(1);
	cube->index(2);

	cube->index(2);
	cube->index(3);
	cube->index(0);

	//---------left face ---------------
	cube->index(4);
	cube->index(0);
	cube->index(3);

	cube->index(3);
	cube->index(7);
	cube->index(4);

	//--------Right Face --------------
	cube->index(1);
	cube->index(6);
	cube->index(5);

	cube->index(5);
	cube->index(2);
	cube->index(1);

	// ----------------Back Face ----------------
	cube->index(6);
	cube->index(4);
	cube->index(7);



	cube->index(7);
	cube->index(5);
	cube->index(6);


	//-------------------Top face--------
	cube->index(3);
	cube->index(2);
	cube->index(5);

	cube->index(5);
	cube->index(7);
	cube->index(3);
	//-------------Bot face------------------
	cube->index(0);
	cube->index(6);
	cube->index(1);

	cube->index(6);
	cube->index(0);
	cube->index(4);
	cube->end();
	return cube;
}*/

//cubeNode= mSceneMgr->getRootSceneNode()->createChildSceneNode();
//cubeNode->attachObject(cubeA);
//cubeNodeA = mSceneMgr->getRootSceneNode()->createChildSceneNode();
//cubeNodeA->attachObject(cubeB);

//ManualObject* cubeA = creatCube(20.0f,ColourValue::Blue,ColourValue::Blue);
//ManualObject* cubeB = creatCube(50.0f, ColourValue::Green, ColourValue::Red);

//Mercury->setRotation(20 * evt.timeSinceLastFrame);
//Degree rotation = Degree(200 *evt.timeSinceLastFrame);
//planetNode->rotate(Vector3(0, 1, 0), Radian(20* evt.timeSinceLastFrame));