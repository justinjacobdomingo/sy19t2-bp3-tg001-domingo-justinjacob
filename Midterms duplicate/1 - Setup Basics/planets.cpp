#include "planets.h"
#include "TutorialApplication.h"
#include <OgreSceneManager.h>
#include <string>

using namespace std;
using namespace Ogre;

planets::planets()
{
	x = 0;
	y = 0;
	z = 0;


}

planets::planets(SceneNode* node)
{
	planetNode = node;
}


planets* planets::createPlanet(SceneManager& sceneManager, float given, ColourValue colour,float x, float z , planets* givenPlanet , bool moon) 
{
	ManualObject* cube = sceneManager.createManualObject();
	cube->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);

	//------front points-----------
	cube->position(-given, -given, given); // i-0 LL 
	cube->colour(colour);
	cube->position(given, -given, given); // i -1 LR
	cube->colour(colour);
	cube->position(given, given, given); // i -2 UR
	cube->colour(colour);
	cube->position(-given, given, given);// i - 3 UL
	cube->colour(colour);

	//----------- back points ----------------
	cube->position(-given, -given, -given); // i-4 LL
	cube->colour(colour);
	cube->position(given, given, -given); // i -5 UR
	cube->colour(colour);
	cube->position(given, -given, -given); // i - 6 LR
	cube->colour(colour);
	cube->position(-given, given, -given); // i - 7 UL
	cube->colour(colour);
	//------front face------------------
	cube->index(0);
	cube->index(1);
	cube->index(2);

	cube->index(2);
	cube->index(3);
	cube->index(0);

	//---------left face ---------------
	cube->index(4);
	cube->index(0);
	cube->index(3);

	cube->index(3);
	cube->index(7);
	cube->index(4);

	//--------Right Face --------------
	cube->index(1);
	cube->index(6);
	cube->index(5);

	cube->index(5);
	cube->index(2);
	cube->index(1);

	// ----------------Back Face ----------------
	cube->index(6);
	cube->index(4);
	cube->index(7);



	cube->index(7);
	cube->index(5);
	cube->index(6);


	//-------------------Top face--------
	cube->index(3);
	cube->index(2);
	cube->index(5);

	cube->index(5);
	cube->index(7);
	cube->index(3);
	//-------------Bot face------------------
	cube->index(0);
	cube->index(6);
	cube->index(1);

	cube->index(6);
	cube->index(0);
	cube->index(4);
	cube->end();
	if (moon == true)
	{
		planetNode = givenPlanet->getPlanetNode()->createChildSceneNode();
		planetNode->attachObject(cube);
		planetNode->setPosition(x, 0, z);
	}
	else
	{
		planetNode = sceneManager.getRootSceneNode()->createChildSceneNode();
		planetNode->attachObject(cube);
		planetNode->setPosition(x, 0, z);
	}

	return new planets(planetNode);
}



void planets::setRotation(float given)
{
	Degree rotation = Degree(given);
	planetNode->rotate(Vector3(0, 1, 0), Radian(rotation));	
}

void planets::updateRevolution(float days, float rotationSpeed)
{
	float given = 365 / days * Math::PI / 180 * rotationSpeed ;
	z = planetNode->getPosition().x * (-Math::Sin(given)) + planetNode->getPosition().z * Math::Cos(given);
	x = planetNode->getPosition().x * (Math::Cos(given)) + planetNode->getPosition().z * Math::Sin(given);
	planetNode->setPosition(x, y, z);
}

float planets::getX()
{
	return planetNode->getPosition().x;
}

float planets::getZ()
{
	return planetNode->getPosition().z;
}

SceneNode* planets::getPlanetNode()
{
	return planetNode;
}

/*Degree rotation = Degree(given);
planetNode->rotate(Vector3(0, 1, 0), Radian(Degree(given)));*/


//ManualObject* cube = mSceneMgr->createManualObject();\

/*if (givenPlanet != nullptr)
{
	location.x = planetNode->getPosition().x - givenPlanet->getPlanetNode()->getPosition().x;
	location.z = planetNode->getPosition().z - givenPlanet->getPlanetNode()->getPosition().z;

	float oldX = location.x;
	float oldZ = location.z;
	z = oldX * -(Math::Sin(given)) + oldZ * Math::Cos(given);
	x = oldX * (Math::Cos(given)) + oldZ * Math::Sin(given);
	planetNode->setPosition(x, y, z);
}*/