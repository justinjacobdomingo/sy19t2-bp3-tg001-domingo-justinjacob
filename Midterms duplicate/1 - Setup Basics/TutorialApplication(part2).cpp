/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	ManualObject * cube = mSceneMgr->createManualObject();
	cube->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);

	//FrontFaceBotRight
	cube->position(0, 0, 0);
	cube->position(20, 0, 0);
	cube->position(20, 20, 0);

	//FrontFaceTopLeft
	cube->position(20,20,0);
	cube->position(0,20,0);
	cube->position(0,0,0);

	//leftFacebotRight
	cube->position(0, 0, -20);
	cube->position(0, 0, 0);
	cube->position(0, 20, 0);

	//leftFacetopLeft
	cube->position(0, 20, 0);
	cube->position(0, 20, -20);
	cube->position(0, 0,-20);

	//RightFaceBotRight
	cube->position(20, 0, 0);
	cube->position(20, 0, -20);
	cube->position(20, 20, -20);

	//RightFaceTopLeft
	cube->position(20, 20, -20);
	cube->position(20, 20, 0);
	cube->position(20, 0, 0);
	
	//BackFaceBotRight
	cube->position(0, 0, -20);
	cube->position(20, 20, -20);
	cube->position(20, 0, -20);
	

	//backFaceTopLeft
	cube->position(20, 20, -20);
	cube->position(0, 0, -20);
	cube->position(0, 20, -20);
	
	//topFacebotRight
	cube->position(0, 20, 0);
	cube->position(20, 20, 0);
	cube->position(20, 20, -20);	

	//topFacetopLeft
	cube->position(20,20,-20);
	cube->position(0, 20, -20);
	cube->position(0, 20, 0);


	//botFacebotRight
	cube->position(0, 0, 0);
	cube->position(20, 0, -20);
	cube->position(20, 0, 0);
	

	//botFacetopLeft
	cube->position(20, 0, -20);
	cube->position(0, 0, 0);
	cube->position(0, 0, -20);
	
	
	

	cube->end();
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(cube);
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
