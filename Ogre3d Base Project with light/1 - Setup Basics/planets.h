#pragma once
#include <string>
#include <OgreManualObject.h>
#include "TutorialApplication.h"

using namespace Ogre;

class planets
{
public:
	planets();
	planets(SceneNode* node);
	planets* createPlanet(SceneManager& sceneManager, float size, ColourValue colour, float x, float z, planets* givenPlane,bool moon);
	
	void setRotation(float given);
	void updateRevolution(float days, float rotationSpeed);


	float getX();
	float getZ();

	SceneNode* getPlanetNode();
	SceneNode* planetNode; 


protected:
	float x;
	float y;
	float z;

};



